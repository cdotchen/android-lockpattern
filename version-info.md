# `8.0.1` _(February 4th, 2016)_

- Support: Android 7+ ([Eclair MR1][Android-Eclair-MR1])

---

## Dependencies

- [`org.bitbucket.haibison:underdogs:+`][#org.bitbucket.haibison:underdogs]


## Changes

- `LockPatternActivity`: fixed `UnsupportedOperationException` while checking for incoming action.


[Android-Eclair-MR1]: https://developer.android.com/reference/android/os/Build.VERSION_CODES.html#ECLAIR_MR1

[#com.android.support:*]: https://developer.android.com/tools/support-library/index.html
[#org.bitbucket.haibison:underdogs]: https://bitbucket.org/haibison/underdogs
